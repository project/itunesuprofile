<?php

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function itunesuprofile_profile_details() {
  return array (
    'name' => 'iTunesU Manager',
    'description' => 'Application to create, edit and manage podcasts.',
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during
 *   the final stage. The keys of the array will be used internally,
 *   while the values will be displayed to the user in the installer
 *   task list.
 */
function itunesuprofile_profile_task_list() {
  $tasks = array(
    'requirements' => st('Verify requirements'),
    'enable modules' => st('Enable modules'),
    'itunesuprofile-ftp' => st('Configure optional remote FTP storage'),
  );
  return $tasks;
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function itunesuprofile_profile_modules() {
  return array(
    // Enable required core modules first.
    'block', 'filter', 'node', 'system', 'user', 'dblog',

    // Enable optional core modules next.
    'help', 'menu', 'taxonomy', 'path', 'php',

    // Install profile API
    'install_profile_api',

    // Contrib modules

    // CCK
    'content', 'content_taxonomy', 'content_taxonomy_autocomplete', 'text', 'nodereference',
    'filefield', 'filefield_meta', 'link', 'optionwidgets', 'filefieldftpdest',

    // Other
     'jquery_ui', 'active_tags', 'arrange_fields',  'getid3',  'admin_menu', 'logintoboggan',

    // Views
    'views', 'views_customfield', 'draggableviews', 'views_ui',

  );
}

function _itunesuprofile_additional_modules() {
  return array(
    'ctools',
    'page_manager',
    'panels', 'panels_node',
    // Features
    'features', 'fe_block', 'fe_taxonomy',
    'strongarm',

    // This ties it all together
    'itunes_feed_builder', 'itunesu_importer'
  );
}

/**
 * Perform any final installation tasks for this profile.
 *
 * The installer goes through the profile-select -> locale-select
 * -> requirements -> database -> profile-install-batch
 * -> locale-initial-batch -> configure -> locale-remaining-batch
 * -> finished -> done tasks, in this order, if you don't implement
 * this function in your profile.
 *
 * If this function is implemented, you can have any number of
 * custom tasks to perform after 'configure', implementing a state
 * machine here to walk the user through those tasks. First time,
 * this function gets called with $task set to 'profile', and you
 * can advance to further tasks by setting $task to your tasks'
 * identifiers, used as array keys in the hook_profile_task_list()
 * above. You must avoid the reserved tasks listed in
 * install_reserved_tasks(). If you implement your custom tasks,
 * this function will get called in every HTTP request (for form
 * processing, printing your information screens and so on) until
 * you advance to the 'profile-finished' task, with which you
 * hand control back to the installer. Each custom page you
 * return needs to provide a way to continue, such as a form
 * submission or a link. You should also set custom page titles.
 *
 * You should define the list of custom tasks you implement by
 * returning an array of them in hook_profile_task_list(), as these
 * show up in the list of tasks on the installer user interface.
 *
 * Remember that the user will be able to reload the pages multiple
 * times, so you might want to use variable_set() and variable_get()
 * to remember your data and control further processing, if $task
 * is insufficient. Should a profile want to display a form here,
 * it can; the form should set '#redirect' to FALSE, and rely on
 * an action in the submit handler, such as variable_set(), to
 * detect submission and proceed to further tasks. See the configuration
 * form handling code in install_tasks() for an example.
 *
 * Important: Any temporary variables should be removed using
 * variable_del() before advancing to the 'profile-finished' phase.
 *
 * @param $task
 *   The current $task of the install system. When hook_profile_tasks()
 *   is first called, this is 'profile'.
 * @param $url
 *   Complete URL to be used for a link or form action on a custom page,
 *   if providing any, to allow the user to proceed with the installation.
 *
 * @return
 *   An optional HTML string to display to the user. Only used if you
 *   modify the $task, otherwise discarded.
 */
function itunesuprofile_profile_tasks(&$task, $url) {
  global $profile, $install_locale;

  watchdog('itunesuprofile', $task);

  if ($task == 'profile') {
    $task = 'requirements';
  }
  if ($task == 'requirements') {
    // Check for getid3 library.
    $getid3_path = "sites/all/libraries/getid3";
    $ok_getid3_exists = file_exists($getid3_path . '/getid3/getid3.lib.php');
    // Check if demos directory not present (Security problem)
    $ok_getid3_secure = TRUE;
    if ($ok_getid3_exists) {
      if (file_exists($getid3_path . '/demos')) {
        $ok_getid3_secure = FALSE;
      }
    }
    // If everything is OK, continue.
    if ($ok_getid3_exists && $ok_getid3_secure) {
      $task = 'enable modules';
    }
    else {
      // Things are missing, so show the confirmation form.
      $output = drupal_get_form('_itunesuprofile_requirements_form');
      $tasks = itunesuprofile_profile_task_list();
      drupal_set_title($tasks[$task]);
      return $output;
    }
  }

  if ($task == 'enable modules') {
    // Install the base module set.
    install_include(itunesuprofile_profile_modules());

    // Set the default theme.
    system_theme_data();
    install_enable_theme("itunesy");
    install_default_theme("itunesy");

    // Enable much of the architecture via features.
    watchdog('itunesuprofile', 'enabling feature modules');
    $modules =  _itunesuprofile_additional_modules();
    foreach ($modules as $module) {
      _drupal_install_module($module);
      module_enable(array($module));
    }

    // Force panel node page to enable
    variable_set('page_manager_node_view_disabled', 0);
    menu_rebuild();

    // Logintobbogan settings.
    variable_set('site_403', 'toboggan/denied');

    // Set up blocks.
    system_initialize_theme_blocks('itunesy');
    $items = array();
    $theme = 'itunesy';
    // All the following blocks are in left sidebar.
    $region = 'left';
    $format = 3;

    // Custom block for "back"
    $content = '<?php
$items = array();

// If current node has parent, show link to parent node.
if (arg(0) == "node" && is_numeric(arg(1))) {
  $nid = arg(1);
  $node = node_load(arg(1));
  if ($node->field_parent) {
    foreach ($node->field_parent as $value) {
      $node2 = node_load($value[\'nid\']);
      if ($node2->nid) {
        #$items[] = "<a href=\'/node/{$node2->nid}\' class=\'back\'>" . t(\'Back to @title\', array(\'@title\' => $node2->title)) . \'</a>\';
        $items[] = l(
          t(\'Back to @title\', array(\'@title\' => $node2->title)),
          "node/{$node2->nid}",
          array(\'attributes\' => array(\'class\' => \'back\'))
        );
        unset($node2);
      }
    }
  }
  unset($node);
}


// If adding child node, show link to parent
if (arg(0) == "node" && arg(1) == "add" && arg(2) == "podcast-item" && is_numeric(arg(3))) {
  $node2 = node_load(arg(3));
  #$items[] = "<a href=\'/node/{$node2->nid}\' class=\'back\'>" . t(\'Back to @title\', array(\'@title\' => $node2->title)) . \'</a>\';
  $items[] = l(
    t(\'Back to @title\', array(\'@title\' => $node2->title)),
    "node/{$node2->nid}",
    array(\'attributes\' => array(\'class\' => \'back\'))
  );
  unset($node2);
}
if ($items) {
  echo implode("\\n", $items);
}
?>';
    $bid = install_create_custom_block($content, "back", $format);
    $delta = sprintf('%d', $bid);
    $items['back'] = new stdClass;
    $items['back']->module = 'block';
    $items['back']->delta = $delta;
    $items['back']->weight = -6;
    $items['back']->region = $region;

    // Custom block for view all items
    $content = '<?php
echo l(t(\'View all items\'), \'<front>\', array(\'attributes\' => array(\'class\' => \'home\')));
?>';
    $bid = install_create_custom_block($content, "home", $format);
    $delta = sprintf('%d', $bid);
    $items['home'] = new stdClass;
    $items['home']->module = 'block';
    $items['home']->delta = $delta;
    $items['home']->weight = -5;
    $items['home']->region = $region;

    $content = '<?php
$msg = t(\'Create new collection\');
$link = l($msg, \'node/add/podcast-collection\', array(\'attributes\' => array(\'class\' => \'add\')));
echo $link;
?>';
    $bid = install_create_custom_block($content, "new_collection", $format);
    $delta = sprintf('%d', $bid);
    $items['new_collection'] = new stdClass;
    $items['new_collection']->module = 'block';
    $items['new_collection']->delta = $delta;
    $items['new_collection']->weight = -4;
    $items['new_collection']->region = $region;

    // Custom block for import
    $content = '<?php
$msg = t(\'Import existing podcast\');
$link = l($msg, \'itunesuimport\', array(\'attributes\' => array(\'class\' => \'import\')));
echo $link;
?>';
    $bid = install_create_custom_block($content, "import", $format);
    $delta = sprintf('%d', $bid);
    $items['import'] = new stdClass;
    $items['import']->module = 'block';
    $items['import']->delta = $delta;
    $items['import']->weight = -3;
    $items['import']->region = $region;

    // Configure all the blocks.
    install_init_blocks();
    foreach ($items as $item) {
      $block = install_set_block($item->module, $item->delta, $theme, $item->region, $item->weight, $item->visibility, $item->pages, $item->custom, $item->throttle, $item->title);
    }

    // Set site footer
    // Commented; this is done by strongarm inside our main feature.
    //variable_set('site_footer', '');

    watchdog('itunesuprofile', 'Finished site configuration');
    menu_rebuild();

    // Hand control back to installer.
    module_rebuild_cache();
    drupal_flush_all_caches();
    features_rebuild();

    $task = 'itunesuprofile-ftp';
  }

  if ($task == 'itunesuprofile-ftp')  {
    $output = drupal_get_form('_itunesuprofile_ftp_form', $url);
    if (!variable_get('fftpd_ftp_config_done', FALSE)) {
      $tasks = itunesuprofile_profile_task_list();
      drupal_set_title($tasks[$task]);
      return $output;
    }
    else {
      variable_del('fftpd_ftp_config_done');
      $task = 'profile-finished';
      variable_set('install_task', 'profile-finished');
    }
  }

}

function _itunesuprofile_requirements_form() {
  $form = array();
  // This is important for all forms during install.
  $form['#redirect'] = FALSE;

  $msg = "<ul>";
  $msg .= "<li>" . st('You must install <a href="@url">getID3()</a> to %dir.', array('@url' => 'http://www.getid3.org', '%dir' => 'sites/all/libraries/getid3'));
  $msg .= "<li>" . st('You must remove the sites/all/libraries/getid3/demos subfolder.');
  $msg .= "</ul>";
  $form['help'] = array(
    '#value' => st('Before finishing, you must do some manual tasks. Please follow these steps: !steps', array('!steps' => $msg)),
  );
  $form['op'] = array(
    '#type' => 'submit',
    '#value' => st('I have done these tasks.') . ' ' . st('Continue'),
  );
  return $form;
}

function _itunesuprofile_ftp_form() {
  $form = array();
  // This is important for all forms during install.
  $form['#redirect'] = FALSE;
  $form['help'] = array(
    '#value' => st('You can optionally configure a remote FTP server that will hold your uploads instead of using the local filesystem. You can leave these blank and configure them later on.'),
  );
  $form['fffd_ftp_host'] = array(
    '#title' => st('FTP hostname'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fffd_ftp_host', ''),
    '#size' => 30,
  );
  $form['fffd_ftp_user'] = array(
    '#title' => st('FTP username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fffd_ftp_user', ''),
    '#size' => 30,
  );
  $form['fffd_ftp_pass'] = array(
    '#title' => st('FTP password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fffd_ftp_pass', ''),
    '#size' => 30,
  );
  $form['fffd_ftp_path'] = array(
    '#title' => st('FTP path'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fffd_ftp_path', '/'),
    '#size' => 30,
    '#description' => st('Should begin with "/". Do not place an ending slash. Example: /media'),
  );
  $form['fffd_ftp_url'] = array(
    '#title' => st('Base URL for the above path'),
    '#type' => 'textfield',
    '#default_value' => variable_get('fffd_ftp_url', ''),
    '#size' => 60,
    '#description' => st('The publicly-available URL where these the above path can be accessed (including the above path if necessary). Do not add a trailing slash. Examples: http://example.com/media or ftp://example.com/media'),
  );
  $form['op'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );
  return $form;
}

function _itunesuprofile_ftp_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Test connectivity. Only validate if some values given.
  if ($values['fffd_ftp_host'] && $values['fffd_ftp_user']) {
    $ftp_settings = array(
      'host' => $values['fffd_ftp_host'],
      'username' => $values['fffd_ftp_user'],
      'password' => $values['fffd_ftp_pass'],
      'directory' => $values['fffd_ftp_path'],
    );
    $ok = _fffd_ftp_init($ftp_settings);
    if (!$ok) {
      form_set_error('fffd_ftp_host', st('Could not connect to FTP server'));
      $ftp_error = TRUE;
    }
  }
}

function _itunesuprofile_ftp_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['fffd_ftp_host']
    && $values['fffd_ftp_user']
    && $values['fffd_ftp_pass']
    && $values['fffd_ftp_path']
    && $values['fffd_ftp_url']) {
    variable_set('fffd_ftp_host', $values['fffd_ftp_host']);
    variable_set('fffd_ftp_user', $values['fffd_ftp_user']);
    variable_set('fffd_ftp_pass', $values['fffd_ftp_pass']);
    variable_set('fffd_ftp_path', $values['fffd_ftp_path']);
    variable_set('fffd_ftp_url', $values['fffd_ftp_url']);
    variable_set('fffd_fields:podcast_item:field_mediafile', 'field_link');
    variable_set('fftpd_ftp_config_done', TRUE);
    drupal_set_message(st('Success! FTP connection settings are working.'));
  }
  else {
    variable_set('fftpd_ftp_config_done', TRUE);
    drupal_set_message(st('FTP connection settings skipped.'));
  }
}

/**
 * Implementation of hook_form_alter().
 */
function itunesuprofile_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    $form['site_information']['site_name']['#default_value'] = 'iTunesU Manager';
    $form['site_information']['site_mail']['#default_value'] = 'admin@'. $_SERVER['HTTP_HOST'];
    $form['admin_account']['account']['name']['#default_value'] = 'admin';
    $form['admin_account']['account']['mail']['#default_value'] = 'admin@'. $_SERVER['HTTP_HOST'];

    /*
    $form['admin_account']['account']['pass']['#default_value'] = 'admin';
    $form['admin_account']['account']['pass']['#type'] = 'textfield';
    $form['admin_account']['account']['pass_2']['#default_value'] = 'admin';
    $form['admin_account']['account']['pass_2']['#type'] = 'textfield';
     *
     */
  }
}

// Set itunesuprofile as default profile.
// (from Atrium installer: This is a trick for hooks to get called, otherwise we cannot alter forms.)
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'itunesuprofile';
  }
}
