<?php

$description = check_plain(strip_tags($node->field_summary[0]['value']));
$subtitle = check_plain(strip_tags($node->field_subtitle[0]['value']));
$enclosure_url = $node->field_link[0]['url'];
$author = check_plain(strip_tags($node->field_authors[0]['value']));

// Get file metadata from filefieldftpdest node info
if ($node->filefieldftpdest['field_mediafile'][0]) {
  $file = $node->filefieldftpdest['field_mediafile'][0];
  $enclosure_size = $file['filesize'];
  $enclosure_mime_type = $file['filemime'];
  $duration = theme('filefield_meta_duration', $file['data']['duration']);
}
else {
  $duration = $node->field_duration[0]['value'];
  if ($enclosure_url) {
    // Try to get data.
    $result = drupal_http_request($enclosure_url, array(), 'HEAD');
    #print_r($result);
    $enclosure_size = $result->headers['Content-Length'];
    $enclosure_mime_type = $result->headers['Content-Type'];
  }
}

// Keywords
$keywords = array();
foreach ($node->field_keywords as $field) {
  if ($field['value']) {
    $term = taxonomy_get_term($field['value']);
    $keywords[] = $term->name;
  }
}
$keywords = implode(',', $keywords);

// Categories
$tmp = array();
foreach ($node->field_category as $field) {
  if ($field['value']) {
    $tmp[] = $field['value'];
  }
}
$categories = '';
foreach ($tmp as $value) {
  $categories .= "<itunesu:category itunesu:code=\"$value\" />";
}

$explicit = "no";

# TODO: use parent's "categories" if item does not have any
# TODO: use parent's "author" if item does not have any
# TODO: use parent's "explicit" if item does not have any

$item_elements = preg_replace('@<dc:creator>.*</dc:creator>@', '', $item_elements);
$item_elements = preg_replace('@<dc:creator />@', '', $item_elements);
$item_elements = preg_replace('@<category domain=".*?">@', '<category>', $item_elements);

?>

    <item>
      <title><?php print $title; ?></title>
      <link><?php print $link; ?></link>
      <description><?php print $description; ?></description>
      <?php print $item_elements; ?>
      <itunes:explicit><?=$explicit?></itunes:explicit>
      <itunes:subtitle><?php print $subtitle; ?></itunes:subtitle>
      <itunes:keywords><?php print $keywords ?></itunes:keywords>
      <itunes:summary><?php print $description ?></itunes:summary>
      <itunes:author><?php print $author?></itunes:author>
      <itunes:order><?php print $order ?></itunes:order>
      <itunes:duration><?php print $duration ?></itunes:duration>
      <enclosure url="<?php print $enclosure_url ?>" length="<?php print $enclosure_size ?>" type="<?php print $enclosure_mime_type ?>" />
      <?php print $categories; ?>
      
    </item>
