<?php

//     <atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="self" href="<?php print $link ? >" type="application/rss+xml" />

// Get information for the collection from the URL.
$nid = arg(1);
$node = node_load($nid);

$channel_language = $node->field_language[0]['value'];
$channel_author = $node->field_authors[0]['value'];
$channel_description = $node->field_summary[0]['value'];
$channel_subtitle = $node->field_subtitle[0]['value'];
$channel_link = url("node/$nid/itunesfeed", array('absolute' => TRUE));
$channel_explicit = $node->field_explicit[0]['value'];
$channel_copyright = "(C)" . date("Y") . " Tecnológico de Monterrey";

$channel_keywords = array();
foreach ($node->field_keywords as $field) {
  if ($field['value']) {
    $term = taxonomy_get_term($field['value']);
    $channel_keywords[] = $term->name;
  }
}
$channel_keywords = implode(',', $channel_keywords);

$tmp = array();
foreach ($node->field_category as $field) {
  if ($field['value']) {
    $tmp[] = $field['value'];
  }
}
$channel_categories = '';
foreach ($tmp as $value) {
  $channel_categories .= "<itunesu:category itunesu:code=\"$value\" />";
}

// Channel image
$channel_image_url = $node->field_image[0]['url'];
$channel_image_title = "";
$channel_image_link = "";

// Channel owner
$uid = $node->uid;
if (!$uid) { $uid = 1; }
$user = user_load($uid);
$channel_owner_name = $user->name;
$channel_owner_email = $user->mail;

?>
<?php print "<?xml"; ?> version="1.0" encoding="utf-8" <?php print "?>"; ?>
<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:itunesu="http://www.itunesu.com/feed">
  <channel>
    <title><?php print $title; ?></title>
    <link><?php print $channel_link; ?></link>
    <description><?php print $channel_subtitle; ?></description>
    <language><?php print $channel_language; ?></language>
    <copyright><?php print $channel_copyright; ?></copyright>
    <itunes:author><?php print $channel_author; ?></itunes:author>
    <itunes:explicit><?php print $channel_explicit; ?></itunes:explicit>
    <itunes:keywords><?php print $channel_keywords; ?></itunes:keywords>
    <itunes:image href="<?php print $channel_image_url; ?>" />
    <?php if ($channel_image_title && $channel_image_link) : ?>
    <image>
      <url><?php print $channel_image_url; ?></url>
      <title><?php print $channel_image_title; ?></title>
      <link><?php print $channel_image_link; ?></link>
    </image>
    <?php endif ?>
    <itunes:owner>
      <itunes:name><?php print $channel_owner_name; ?></itunes:name>
      <itunes:email><?php print $channel_owner_email; ?></itunes:email>
    </itunes:owner>
    <itunes:subtitle><?php print $channel_subtitle; ?></itunes:subtitle>
    <itunes:summary><?php print $channel_description; ?></itunes:summary>
    <?php print $channel_categories; ?>
    <?php print $channel_elements; ?>
    <?php print $items; ?>
  </channel>
</rss>
