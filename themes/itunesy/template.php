<?php

/**
 * Default theme function for all RSS rows.
 */
function itunesy_preprocess_views_view_row_rss(&$vars) {
  $view = $vars['view'];
  $options = $vars['options'];
  $item = $vars['row'];

  // Use the [id] of the returned results to determine the nid in [results]
  $result = $vars['view']->result;
  $id = $vars['id'];
  $nid = $result[$id-1]->nid;
  $node = node_load($nid);

  $vars['title'] = check_plain($item->title);
  $vars['link'] = check_url($item->link);
  $vars['description'] = check_plain($item->description);
  $vars['item_elements'] = empty($item->elements) ? '' : format_xml_elements($item->elements);
  // pass entire node object to template
  $vars['node'] = $node;
  
  // pass the order for this node
  $order = 0;
  foreach ($result as $values) {
    if ($values->nid == $nid) {
      $order = $values->draggableviews_structure_node0_value;
    }
  }
  $vars['order'] = $order;
}

/**
* Override node form
*/
function phptemplate_node_form($form) {
  // Remove 'Log message' text area
  $form['revision_information']['#access'] = FALSE;
  return theme_node_form($form);
}
