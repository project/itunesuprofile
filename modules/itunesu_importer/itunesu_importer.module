<?php

/**
 * @file 
 */



/**
 * Implementation of hook_help()
 */
function itunesu_importer_help($path, $arg) {

}

/**
 * Implementation of hook_menu()
 */
function itunesu_importer_menu() {
  $items=array();
  $items['itunesuimport'] = array(
	  'title' => t('Import an iTunes feed'),
    'description' => '',
	  'page callback' => 'itunesu_importer_importpage',
    'type' => MENU_CALLBACK,
	  'access arguments' => array('create podcast_collection content'),
	);
  // Path for "publish feed to FTP" functionality.
  $items['itunesexport/%'] = array(
    'title' => t('Publish an iTunes feed'),
    'description' => '',
	  'page callback' => 'itunesu_importer_publish_page',
    'page arguments' => array(1),
    'type' => MENU_CALLBACK,
    'access callback' => 'itunesu_importer_itunesexport_access',
  );
	return $items;
}

/**
 * Returns TRUE if current user has permissions to publish feeds via FTP.
 */
function itunesu_importer_itunesexport_access() {
  global $user;
  if (user_access('administer content') || isset($user->roles[4])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Return HTML page (with confirmation form) when the "publish feed to FTP" path
 * is requested by user.
 * @see itunesu_importer_menu()
 */
function itunesu_importer_publish_page($nid) {
  // Get default FTP settings.
  $ftp_settings = _fffd_ftp_settings($nid);
  // Check the settings are valid.
  if (!_fffd_ftp_settings_valid($ftp_settings)) {
    drupal_set_message(t('FTP settings are not yet configured.'), 'error');
    drupal_goto("node/" . $nid);
  }
  $node = node_load($nid);
  if (!$node) {
    return t('The given node does not exist');
  }
  $content = drupal_get_form('itunesu_importer_publish_form', $node);
  return $content;
}

/**
 * Returns a confirmation form.
 * @see itunesu_importer_publish_page()
 */
function itunesu_importer_publish_form(&$form_state, $node) {
  $form = array();
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $ftp_settings = _fffd_ftp_settings($node->nid);
  return confirm_form(
    $form,
    t('Are you sure you want to publish feed for %xyz', array('%xyz' => $node->title)),
    'node/' . $node->nid, // Cancel path
    t('This will export the current collection and track information onto the FTP folder %directory.', array('%directory' => $ftp_settings['directory']))
  );
}

/**
 * Submit function for itunesu_importer_publish_form, pushes the given collection
 * feed onto FTP.
 * 
 * @see itunesu_importer_publish_form()
 */
function itunesu_importer_publish_form_submit($form_state) {
  // Get default FTP settings.
  $ftp_settings = _fffd_ftp_settings($form_state['nid']['#value']);
  if (!_fffd_ftp_settings_valid($ftp_settings)) {
    drupal_set_message(t('FTP settings are not yet configured.'), 'error');
    drupal_goto("node/" . $form_state['nid']['#value']);
  }

  // Donwload the feed XML.
  $feed_url = url('node/' . $form_state['nid']['#value'] . '/itunesfeed', array('absolute' => TRUE, 'query' => time()));
  $result = drupal_http_request($feed_url);
  #dsm($feed_url);
  if ($result->code != 200) {
    drupal_set_message(t('Could not generate feed for node.'), 'error');
    drupal_goto("node/" . $form_state['nid']['#value']);
  }

  // Create filename to store the downloaded feed.
  // Filename should be same as dest ftp folder + .xml
  $filename = pathinfo($ftp_settings['directory'], PATHINFO_BASENAME) . '.xml'; 
  $local_file = file_directory_temp() . '/' . $filename;
  $ok = file_save_data($result->data, $local_file, FILE_EXISTS_REPLACE);
  if (!$ok) {
    drupal_set_message(t('Could not save temporary file.'), 'error');
    drupal_goto("node/" . $form_state['nid']['#value']);
  }

  // Upload the file.
  $ftp = _fffd_ftp_init($ftp_settings);
  // TODO: Check for FTP errors.
  $ok = _fffd_ftp_put($ftp, $filename, array('filepath' => $local_file));
  if ($ok) {
    $final_url = "http://" . $ftp_settings['host'] . $ftp_settings['directory'] . "/" . drupal_urlencode($filename);
    drupal_set_message(t('Successfully published feed to FTP directory: !link', array('!link' => l(t('view uploaded file'), $final_url, array('query' => time())))));
  }
  else {
    drupal_set_message(t('Could not publish feed via FTP.'), 'error');
  }
  drupal_goto("node/" . $form_state['nid']['#value']);
}


/**
 * Helper function that returns the FTP path for a certain collection nid.
 */
function _itunesu_importer_getdir($nid) {
  $nids_dirs = variable_get('itunesu_importer_nids_dirs', array());
  return @$nids_dirs[$nid];
}

/**
 * Implementation of hook_form_alter()
 * Add a form element to
 */
function itunesu_importer_form_alter(&$form, $form_state, $form_id) {
  // Alter the node edition form for "podcast_collection" nodetype.
  if ($form_id == "podcast_collection_node_form") {
    // Only add element if current user has permissions.
    if (itunesu_importer_itunesexport_access()) {
      // Determine current configured FTP directory for this node.
      $dir = "";
      if ($form_state['values']['ftpdest']) {
        // Form submitted before reaching this, so use the submitted value.
        $dir = $form_state['values']['ftpdest'];
      }
      else {
        // Get the value from previously saved settings
        $dir = _itunesu_importer_getdir($form['nid']['#value']);
      }
      // Default FTP settings.
      $ftp_settings = _fffd_ftp_settings();
      // Add the element to the form.
      $form['ftpdest'] = array(
        '#type' => 'textfield',
        '#size' => '30',
        '#title' => t('FTP folder for this collection'),
        '#description' => t('Leave empty to use default FTP folder %folder. Make sure this folder exists on the server. This will be checked when saving.', array('%folder' => $ftp_settings['directory'])),
        '#element_validate' => array('itunesu_importer_ftpdest_validate'),
        '#default_value' => $dir,
      );
    }
  }
}

/**
 * element_validate handler for ftpdest element.
 * @see itunesu_importer_form_alter().
 */
function itunesu_importer_ftpdest_validate($element, &$form_state) {
  if (!empty($element['#value'])) {

    // Check directory has not been specified in another node.
    $nids_dirs = variable_get('itunesu_importer_nids_dirs', array());
    $key = array_search($element['#value'], $nids_dirs);
    if (is_numeric($key) && $key != $form_state['values']['nid']) {
      $node2 = node_load($key);
      $link = l($node2->title, "node/$key");
      form_error($element, t('FTP directory @directory is already in use by !link.', array('@directory' => $element['#value'], '!link' => $link)));
    }

    // Check directory exists
    $settings = _fffd_ftp_settings();
    $settings['directory'] = $element['#value'];
    $ok = _fffd_ftp_init($settings);
    if ($ok === FALSE) {
      form_error($element, t('Invalid FTP directory @directory.', array('@directory' => $element['#value'])));
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function itunesu_importer_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'presave':
      break;
    case 'insert':
    case 'update':
      // Store the submitted values in a variable.
      $nids_dirs = variable_get('itunesu_importer_nids_dirs', array());
      $nids_dirs[$node->nid] = $node->ftpdest;
      variable_set('itunesu_importer_nids_dirs', $nids_dirs);
      break;
    case 'view':
      break;
    case 'validate':
      break;
    case 'delete':
      // Remove any value from the variable.
      $nids_dirs = variable_get('itunesu_importer_nids_dirs', array());
      unset($nids_dirs[$node->nid]);
      variable_set('itunesu_importer_nids_dirs', $nids_dirs);
      break;
    case 'prepare':
      break;
    case 'load':
      break;
    case 'default':
      break;
  }
}


/**
 * Show instructions and form to start import.
 */
function itunesu_importer_importpage() {
  drupal_set_title(t('Import an iTunes feed'));
	$output = t('You can import a Feed channel and all of its items so you can edit, reorganize and/or change metadata.');
	$output .= drupal_get_form( 'itunesu_importer_importform' );
	return $output;
}

/**
 * Return a form array.
 */
function itunesu_importer_importform() {
  global $user;
  $form = array();
  $form['url'] = array(
		'#type' => 'textfield',
		'#title' => t('URL of the feed you wish to import'),
		'#description' => t("Type the address of an existing feed."),
		'#default_value' => 'http://',
		'#maxlength' => 4000,
	);
  $result = db_query("SELECT uid, name FROM {users} WHERE status = 1 ORDER BY name");
  $options = array();
  while ($data = db_fetch_object($result)) {
    $options[$data->uid] = $data->name;
  }
  $form['author_uid'] = array(
    '#type' => 'radios',
    '#title' => t("User that will own the created collection and tracks"),
    '#default_value' => $user->uid,
    '#options' => $options,
  );

	$form['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Continue'),
	);
  return $form;

}

function itunesu_importer_importform_validate($form, &$form_state) {
  $url = $form_state['values']['url'];
  if (!valid_url($url, TRUE)) {
    form_set_error('url', t('Please enter a valid URL'));
  }
  $author_uid = $form_state['values']['author_uid'];
  $user = user_load(array('uid' => $author_uid));
  if (!$user) {
    form_set_error('author_uid', t('Please select a user.'));
  }
  $result = _itunesu_importer_parse($url);
  if (!$result) {
    form_set_error('url', t('Could not parse the remote data. Please check the URL points to a valid feed, and that the feed contents are valid XML.'));
  }
}

function itunesu_importer_importform_submit($form, &$form_state) {
  $url = $form_state['values']['url'];
  
  $result = _itunesu_importer_parse($url);

  // Import the items
  $user = user_load(array('uid' => $form_state['values']['author_uid']));
  _itunes_importer_batch_import($result, $user);
}

/**
 * Import parsed channel data as nodes.
 * TODO: Handle taxonomy.
 */
function _itunes_importer_batch_import($channel, $user) {
  // First create the collection node.
  $node = new stdClass();
  $node->title = $channel['title'];
  $node->type = 'podcast_collection';
  $node->uid = $user->uid;
  $node->name = $user->name;

  $textfield_mappings = array('description', 'subtitle', 'explicit', 'category', 'chan_owner_name', 'chan_owner_email', 'summary');
  foreach ($textfield_mappings as $key) {
    $fieldname = "field_$key";
    $node->{$fieldname}[0]['value'] = $channel[$key];
  }
  // Special cases
  $node->field_authors[0]['value'] = $channel['author'];
  $node->field_language[0]['value'] = strtoupper(drupal_substr($channel['language'],0,2));
  $node->field_image[0]['url'] = $channel['image'];
  $node = node_submit($node);
  node_save($node);

  // Create a batch to import all items
  $operations = array();
  foreach ($channel['items'] as $item) {
    $operations[] = array('_itunesu_importer_import_item', array($item, $node->nid, $user));
  }
  $batch = array(
    'operations' => $operations,
    'finished' => '_itunes_importer_batch_import_finished',
    'title' => t('Importing items from feed...'),
    'init_message' => t('Batch processing is starting.'),
    'error_message' => t('Batch importing has encountered an error.'),
  );
  batch_set($batch);
}
/**
 * Callback function when batch finishes.
 * @see _itunes_importer_batch_import().
 */
function _itunes_importer_batch_import_finished($success, $results, $operations) {
  drupal_set_message(t('Successfully imported podcast feed.'));
  drupal_goto("node/" . $results['parent_nid']);
}

function _itunesu_importer_import_item($item, $parent_nid, $user, &$context) {
  $item_node = new stdClass();
  $item_node->title = $item['title'];
  $item_node->type = 'podcast_item';
  $item_node->uid = $user->uid;
  $item_node->name = $user->name;
  $textfield_mappings = array('summary', 'subtitle', 'explicit', 'category', 'duration');
  foreach ($textfield_mappings as $key) {
    $fieldname = "field_$key";
    $item_node->{$fieldname}[0]['value'] = $item[$key];
  }
  // Special cases
  $item_node->field_authors[0]['value'] = $item['author'];
  $item_node->field_link[0]['url'] = $item['url'];
  $item_node->field_parent[0]['nid'] = $parent_nid;
  $item_node = node_submit($item_node);
  node_save($item_node);
  $context['results']['parent_nid'] = $parent_nid;
}

/**
 * Generates an array with data for an iTunes RSS channel.
 * TODO: Handle bad formats.
 */
function _itunesu_importer_parse($url) {
  $xml = @simplexml_load_file($url);
  if (!$xml) {
    return FALSE;
  }

  $channel = array();
  $mappings = array('title', 'language', 'description', 'copyright');
  foreach ($mappings as $mapping) {
    if (is_object($xml->channel->$mapping)) {
      $channel[$mapping] = (string)$xml->channel->$mapping->asXML();
    }
  }
  $channel_itunes_namespace = $xml->channel->
    children('http://www.itunes.com/dtds/podcast-1.0.dtd');
  $mappings = array('subtitle', 'author', 'summary', 'explicit');
  foreach ($mappings as $mapping) {
    $channel[$mapping] = (string)$channel_itunes_namespace->$mapping;
  }
  // Get image
  if (is_object($channel_itunes_namespace->image)) {
    $image = (string)$channel_itunes_namespace->image->asXML();
  }
  $channel['image'] = preg_replace('/.*?href="(.*?)".*/', '\1', $image);
  // Channel owner info
  $channel['chan_owner_name'] = $channel_itunes_namespace->owner->name;
  $channel['chan_owner_email'] = $channel_itunes_namespace->owner->email;
  // Get channel category
  $channel_itunesu_namespace = $xml->channel->children('http://www.itunesu.com/feed');
  if ($channel_itunesu_namespace) {
    $cat = (string)$channel_itunesu_namespace->category->asXML();
    $channel['category'] = preg_replace('/<itunesu:category itunesu:code="([0-9]+)"\/>/', '\1', $cat);
  }

  // Normalize all strings
  foreach ($channel as $key => $value) {
    $channel[$key] = _itunesu_importer_filter($value);
  }


  // Process each <item> element in the response.
  $channel['items'] = array();
  foreach ($xml->channel->item as $xml_item) {
    #dsm($count++);
    $item = array();

    $mappings = array('title', 'guid', 'pubDate');
    foreach ($mappings as $mapping) {
      $item[$mapping] = (string)$xml_item->$mapping->asXML();
    }

    // Enclosure as special case
    $mappings = array('url', 'type', 'length');
    foreach ($mappings as $mapping) {
      $item[$mapping] = (string)$xml_item->enclosure[$mapping];
    }

    $item_itunes_namespace = $xml_item->children('http://www.itunes.com/dtds/podcast-1.0.dtd');
    #dpm($item_itunes_metadata);
    $mappings = array('subtitle', 'author', 'duration', 'keywords', 'summary');
    foreach ($mappings as $mapping) {
      $item[$mapping] = strip_tags((string)$item_itunes_namespace->$mapping->asXML());
    }

    $item_itunesu_namespace = $xml_item->children('http://www.itunesu.com/feed');
    $cat = (string)$item_itunesu_namespace->category->asXML();
    $item['category'] = preg_replace('/<itunesu:category itunesu:code="([0-9]+)"\/>/', '\1', $cat);
    
    // Normalize all strings except summary
    // TODO: Why can't I normalize item summaries?
    foreach ($item as $key => $value) {
      #if ($key == "summary") continue;
      if ($key == "category") continue;
      $item[$key] = _itunesu_importer_filter($value);
    }
    // Summary
    #dsm("Is summary a string? " . (is_string($item['summary'])));
    #$item['summary'] = drupal_substr($item['summary'], 16);

    $channel['items'][] = $item;
  }

  return $channel;
}

/**
 * Returns a trimmed, normalized string without starting/ending XML tags.
 */
function _itunesu_importer_filter($string) {
  mb_regex_encoding("UTF-8");
  // Remove tags
  $string = trim($string);
  $string = mb_ereg_replace('^<.*?\/>$', '', $string, 'm');
  $string = mb_ereg_replace('^<.*?>(.*?)<\/.*?>$', '\1', $string, 'm');
  $string = html_entity_decode($string, ENT_NOQUOTES, 'UTF-8');
  return $string;
}


function _itunesu_importer_getsize($url) {
  if ($url) {
    // Try to get data.
    $result = drupal_http_request($url, array(), 'HEAD');
    #print_r($result);
    $enclosure_size = $result->headers['Content-Length'];
    #$enclosure_mime_type = $result->headers['Content-Type'];
  }
  return $enclosure_size;
}
