<?php
/**
 * Implementation of hook_content_default_fields().
 */
function itunes_feed_builder_content_default_fields() {
  $fields = array();

  // Exported field: field_authors
  $fields['podcast_collection-field_authors'] = array(
    'field_name' => 'field_authors',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '255',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_authors][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Author(s)',
      'weight' => '8',
      'description' => 'The content of this tag is shown in the Artist column in iTunes.<br>255 characters max. Enter multiple author names separated by a comma.<br>Example: John Doe,James Brown,George W. Bush. ',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_category
  $fields['podcast_collection-field_category'] = array(
    'field_name' => 'field_category',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => '6',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '100|Business
100100|Business > Economics
100101|Business > Finance
100102|Business > Hospitality
100103|Business > Management
100104|Business > Marketing
100105|Business > Personal Finance
100106|Business > Real Estate
101|Engineering
101100|Engineering > Chemical & Petroleum
101101|Engineering > Civil
101102|Engineering > Computer Science
101103|Engineering > Electrical
101104|Engineering > Environmental
101105|Engineering > Mechanical
102|Fine Arts
102100|Fine Arts > Architecture
102101|Fine Arts > Art
102102|Fine Arts > Art History
102103|Fine Arts > Dance
102104|Fine Arts > Film
102105|Fine Arts > Graphic Design
102106|Fine Arts > Interior Design
102107|Fine Arts > Music
102108|Fine Arts > Theater
103|Health & Medicine
103100|Health & Medicine > Anatomy & Physiology
103101|Health & Medicine > Behavioral Science
103102|Health & Medicine > Dentistry
103103|Health & Medicine > Diet & Nutrition
103104|Health & Medicine > Emergency
103105|Health & Medicine > Genetics
103106|Health & Medicine > Gerontology
103107|Health & Medicine > Health & Exercise Science
103108|Health & Medicine > Immunology
103109|Health & Medicine > Neuroscience
103110|Health & Medicine > Pharmacology & Toxicology
103111|Health & Medicine > Psychiatry
103112|Health & Medicine > Public Health
103113|Health & Medicine > Radiology
104|History
104100|History > Ancient
104101|History > Medieval
104102|History > Military
104103|History > Modern
104104|History > African
104105|History > Asian
104106|History > European
104107|History > Middle Eastern
104108|History > North American
104109|History > South American
105|Humanities
105100|Humanities > Communications
105101|Humanities > Philosophy
105102|Humanities > Religion
106|Language
106100|Language > African
106101|Language > Ancient
106102|Language > Asian
106103|Language > Eastern European/Slavic
106104|Language > English
106105|Language > English Language Learners
106106|Language > French
106107|Language > German
106108|Language > Italian
106109|Language > Linguistics
106110|Language > Middle Eastern
106111|Language > Spanish & Portuguese
106112|Language > Speech Pathology
107|Literature
107100|Literature > Anthologies
107101|Literature > Biography
107102|Literature > Classics
107103|Literature > Criticism
107104|Literature > Fiction
107105|Literature > Poetry
108|Mathematics
108100|Mathematics > Advanced Mathematics
108101|Mathematics > Algebra
108102|Mathematics > Arithmetic
108103|Mathematics > Calculus
108104|Mathematics > Geometry
108105|Mathematics > Statistics
109|Science
109100|Science > Agricultural
109101|Science > Astronomy
109102|Science > Atmospheric
109103|Science > Biology
109104|Science > Chemistry
109105|Science > Ecology
109106|Science > Geography
109107|Science > Geology
109108|Science > Physics
110|Social Science
110100|Social Science > Law
110101|Social Science > Political Science
110102|Social Science > Public Administration
110103|Social Science > Psychology
110104|Social Science > Social Welfare
110105|Social Science > Sociology
111|Society
111100|Society > African-American Studies
111101|Society > Asian Studies
111102|Society > European & Russian Studies
111103|Society > Indigenous Studies
111104|Society > Latin & Caribbean Studies
111105|Society > Middle Eastern Studies
111106|Society > Women’s Studies
112|Teaching & Education
112100|Teaching & Education > Curriculum & Teaching
112101|Teaching & Education > Educational Leadership
112102|Teaching & Education > Family & Childcare
112103|Teaching & Education > Learning Resources
112104|Teaching & Education > Psychology & Research
112105|Teaching & Education > Special Education',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Category',
      'weight' => '1',
      'description' => 'Choose an iTunes category.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_chan_owner_email
  $fields['podcast_collection-field_chan_owner_email'] = array(
    'field_name' => 'field_chan_owner_email',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_chan_owner_email][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Channel owner email',
      'weight' => '10',
      'description' => 'Enter a valid email. This tag will only be used to contact the owner of the podcast and will not be publicly displayed.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_chan_owner_name
  $fields['podcast_collection-field_chan_owner_name'] = array(
    'field_name' => 'field_chan_owner_name',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_chan_owner_name][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Channel owner name',
      'weight' => '9',
      'description' => 'Complete name of the channel owner.  This tag will only be used to contact the owner of the podcast and will not be publicly displayed.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_collection_thumbnail
  $fields['podcast_collection-field_collection_thumbnail'] = array(
    'field_name' => 'field_collection_thumbnail',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'jpg gif png',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '250K',
      'max_filesize_per_node' => '',
      'label' => 'Collection thumbnail',
      'weight' => '11',
      'description' => '',
      'type' => 'filefield_widget',
      'module' => 'filefield',
    ),
  );

  // Exported field: field_explicit
  $fields['podcast_collection-field_explicit'] = array(
    'field_name' => 'field_explicit',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'yes|yes
no|no
clean|clean',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'no',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Explicit content',
      'weight' => '2',
      'description' => 'This tag should be used to indicate whether or not your podcast contains explicit material.<br />
If you choose "yes", an "explicit" parental advisory graphic will appear next to your podcast artwork on the iTunes Store, and in the Name column in iTunes. "clean" means that no explicit language or adult content is included anywhere in the episodes, and a "clean" graphic will appear. If you choose "no", you will see no indicator.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_image
  $fields['podcast_collection-field_image'] = array(
    'field_name' => 'field_image',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'url',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'url',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'link',
    'required' => '1',
    'multiple' => '0',
    'module' => 'link',
    'active' => '1',
    'attributes' => array(
      'target' => 'default',
      'rel' => '',
      'class' => '',
      'title' => '',
    ),
    'display' => array(
      'url_cutoff' => '80',
    ),
    'url' => 0,
    'title' => 'none',
    'title_value' => '',
    'enable_tokens' => '',
    'validate_url' => 1,
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'url' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Collection image URL',
      'weight' => 0,
      'description' => 'Provide a full URL to the image file for the artwork on your podcast. iTunes:
<ul>
<li>prefers square .jpg images that are at least 600 x 600 pixels, which is different than what is specified for the standard RSS image tag.
<li>supports JPEG and PNG formats. The URL must end in ".jpg" or ".png".
</ul>',
      'type' => 'link',
      'module' => 'link',
    ),
  );

  // Exported field: field_keywords
  $fields['podcast_collection-field_keywords'] = array(
    'field_name' => 'field_keywords',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'link',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'link',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'content_taxonomy',
    'required' => '0',
    'multiple' => '10',
    'module' => 'content_taxonomy',
    'active' => '1',
    'save_term_node' => 1,
    'vid' => '1',
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'widget' => array(
      'new_terms' => 'insert',
      'extra_parent' => '0',
      'maxlength' => '1000',
      'active_tags' => 1,
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Keywords',
      'weight' => '-1',
      'description' => 'You can provide up to 10 keywords that will help iTunes users find your collection.',
      'type' => 'content_taxonomy_autocomplete',
      'module' => 'content_taxonomy_autocomplete',
    ),
  );

  // Exported field: field_language
  $fields['podcast_collection-field_language'] = array(
    'field_name' => 'field_language',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'EN|English American
ES|Spanish',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => 'global $language;
return array(0 => array(\'value\' => strtoupper($language->language)));',
      'mccr_width' => 1,
      'mccr_row-major' => 0,
      'mccr_indent' => 0,
      'mccr_caption' => '',
      'mccr_column-heading' => '',
      'mccr_row-heading' => '',
      'label' => 'Language',
      'weight' => '-2',
      'description' => '',
      'type' => 'optionwidgets_buttons',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_subtitle
  $fields['podcast_collection-field_subtitle'] = array(
    'field_name' => 'field_subtitle',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '255',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_subtitle][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Subtitle',
      'weight' => '-4',
      'description' => 'The contents of this tag are shown in the Description column in iTunes.<br>The subtitle displays best if it is only a few words long. 255 characters max.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_summary
  $fields['podcast_collection-field_summary'] = array(
    'field_name' => 'field_summary',
    'type_name' => 'podcast_collection',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '4000',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_summary][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Summary',
      'weight' => '-3',
      'description' => 'The contents of this tag are shown in a separate window that appears when the "circled i" in the Description column is clicked.<br>It also appears on the iTunes page for your podcast. This field can be up to 4000 characters.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_authors
  $fields['podcast_item-field_authors'] = array(
    'field_name' => 'field_authors',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '255',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_authors][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Author(s)',
      'weight' => '-3',
      'description' => 'The content of this tag is shown in the Artist column in iTunes.<br>Enter multiple author names separated by a comma.<br>Example: John Doe,James Brown,George W. Bush',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_category
  $fields['podcast_item-field_category'] = array(
    'field_name' => 'field_category',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '100|Business
100100|Business > Economics
100101|Business > Finance
100102|Business > Hospitality
100103|Business > Management
100104|Business > Marketing
100105|Business > Personal Finance
100106|Business > Real Estate
101|Engineering
101100|Engineering > Chemical & Petroleum
101101|Engineering > Civil
101102|Engineering > Computer Science
101103|Engineering > Electrical
101104|Engineering > Environmental
101105|Engineering > Mechanical
102|Fine Arts
102100|Fine Arts > Architecture
102101|Fine Arts > Art
102102|Fine Arts > Art History
102103|Fine Arts > Dance
102104|Fine Arts > Film
102105|Fine Arts > Graphic Design
102106|Fine Arts > Interior Design
102107|Fine Arts > Music
102108|Fine Arts > Theater
103|Health & Medicine
103100|Health & Medicine > Anatomy & Physiology
103101|Health & Medicine > Behavioral Science
103102|Health & Medicine > Dentistry
103103|Health & Medicine > Diet & Nutrition
103104|Health & Medicine > Emergency
103105|Health & Medicine > Genetics
103106|Health & Medicine > Gerontology
103107|Health & Medicine > Health & Exercise Science
103108|Health & Medicine > Immunology
103109|Health & Medicine > Neuroscience
103110|Health & Medicine > Pharmacology & Toxicology
103111|Health & Medicine > Psychiatry
103112|Health & Medicine > Public Health
103113|Health & Medicine > Radiology
104|History
104100|History > Ancient
104101|History > Medieval
104102|History > Military
104103|History > Modern
104104|History > African
104105|History > Asian
104106|History > European
104107|History > Middle Eastern
104108|History > North American
104109|History > South American
105|Humanities
105100|Humanities > Communications
105101|Humanities > Philosophy
105102|Humanities > Religion
106|Language
106100|Language > African
106101|Language > Ancient
106102|Language > Asian
106103|Language > Eastern European/Slavic
106104|Language > English
106105|Language > English Language Learners
106106|Language > French
106107|Language > German
106108|Language > Italian
106109|Language > Linguistics
106110|Language > Middle Eastern
106111|Language > Spanish & Portuguese
106112|Language > Speech Pathology
107|Literature
107100|Literature > Anthologies
107101|Literature > Biography
107102|Literature > Classics
107103|Literature > Criticism
107104|Literature > Fiction
107105|Literature > Poetry
108|Mathematics
108100|Mathematics > Advanced Mathematics
108101|Mathematics > Algebra
108102|Mathematics > Arithmetic
108103|Mathematics > Calculus
108104|Mathematics > Geometry
108105|Mathematics > Statistics
109|Science
109100|Science > Agricultural
109101|Science > Astronomy
109102|Science > Atmospheric
109103|Science > Biology
109104|Science > Chemistry
109105|Science > Ecology
109106|Science > Geography
109107|Science > Geology
109108|Science > Physics
110|Social Science
110100|Social Science > Law
110101|Social Science > Political Science
110102|Social Science > Public Administration
110103|Social Science > Psychology
110104|Social Science > Social Welfare
110105|Social Science > Sociology
111|Society
111100|Society > African-American Studies
111101|Society > Asian Studies
111102|Society > European & Russian Studies
111103|Society > Indigenous Studies
111104|Society > Latin & Caribbean Studies
111105|Society > Middle Eastern Studies
111106|Society > Women’s Studies
112|Teaching & Education
112100|Teaching & Education > Curriculum & Teaching
112101|Teaching & Education > Educational Leadership
112102|Teaching & Education > Family & Childcare
112103|Teaching & Education > Learning Resources
112104|Teaching & Education > Psychology & Research
112105|Teaching & Education > Special Education',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Category',
      'weight' => 0,
      'description' => 'Choose an iTunes category.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_duration
  $fields['podcast_item-field_duration'] = array(
    'field_name' => 'field_duration',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '2',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '8',
      'default_value' => array(
        '0' => array(
          'value' => '00:00:00',
          '_error_element' => 'default_value_widget][field_duration][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Duration',
      'weight' => '2',
      'description' => 'Duration in HH:MM:SS or MM:SS',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_keywords
  $fields['podcast_item-field_keywords'] = array(
    'field_name' => 'field_keywords',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'content_taxonomy',
    'required' => '0',
    'multiple' => '10',
    'module' => 'content_taxonomy',
    'active' => '1',
    'save_term_node' => 1,
    'vid' => '1',
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'widget' => array(
      'new_terms' => 'insert',
      'extra_parent' => '0',
      'maxlength' => '1000',
      'active_tags' => 1,
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Keywords',
      'weight' => '-1',
      'description' => 'You can provide up to 10 keywords that will help iTunes users find this item.',
      'type' => 'content_taxonomy_autocomplete',
      'module' => 'content_taxonomy_autocomplete',
    ),
  );

  // Exported field: field_link
  $fields['podcast_item-field_link'] = array(
    'field_name' => 'field_link',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'link',
    'required' => '1',
    'multiple' => '0',
    'module' => 'link',
    'active' => '1',
    'attributes' => array(
      'target' => 'default',
      'rel' => '',
      'class' => '',
      'title' => '',
    ),
    'display' => array(
      'url_cutoff' => '120',
    ),
    'url' => 0,
    'title' => 'value',
    'title_value' => 'link',
    'enable_tokens' => '',
    'validate_url' => 1,
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'url' => 'http://FALTA.URL.com/',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Link to podcast file',
      'weight' => '1',
      'description' => 'Coloca el URL que lleva al archivo de medios.<br>Ejemplo: http://prod77ms.itesm.mx/Podcast&OM/123.m4v',
      'type' => 'link',
      'module' => 'link',
    ),
  );

  // Exported field: field_mediafile
  $fields['podcast_item-field_mediafile'] = array(
    'field_name' => 'field_mediafile',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'mp4 mp3 mov m4v avi flv',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'label' => 'Media file',
      'weight' => '8',
      'description' => '',
      'type' => 'filefield_widget',
      'module' => 'filefield',
    ),
  );

  // Exported field: field_parent
  $fields['podcast_item-field_parent'] = array(
    'field_name' => 'field_parent',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '3',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'podcast_collection' => 'podcast_collection',
      'page' => 0,
      'panel' => 0,
      'podcast_item' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => 60,
      'default_value' => NULL,
      'default_value_php' => '$nid = arg(3);
if (is_numeric($nid)) {
  return array(0 => array(\'nid\' => $nid));
} else {
  return array(0 => array(\'nid\' => FALSE));
}',
      'label' => 'Belongs to collection',
      'weight' => '3',
      'description' => 'Select the podcast collections in which this item should appear.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_subtitle
  $fields['podcast_item-field_subtitle'] = array(
    'field_name' => 'field_subtitle',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '255',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '2',
      'size' => '140',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_subtitle][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Subtitle',
      'weight' => '-4',
      'description' => 'The contents of this tag are shown in the Description column in iTunes.<br>
The subtitle displays best if it is only a few words long.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_summary
  $fields['podcast_item-field_summary'] = array(
    'field_name' => 'field_summary',
    'type_name' => 'podcast_item',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '4000',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_summary][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Summary',
      'weight' => '-2',
      'description' => 'The contents of this tag are shown in a separate window that appears when the "circled i" in the Description column is clicked.<br>It also appears on the iTunes page for your podcast. This field can be up to 4000 characters.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Author(s)');
  t('Belongs to collection');
  t('Category');
  t('Channel owner email');
  t('Channel owner name');
  t('Collection image URL');
  t('Collection thumbnail');
  t('Duration');
  t('Explicit content');
  t('Keywords');
  t('Language');
  t('Link to podcast file');
  t('Media file');
  t('Subtitle');
  t('Summary');

  return $fields;
}
