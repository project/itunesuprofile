<?php
/**
 * Implementation of hook_user_default_permissions().
 */
function itunes_feed_builder_user_default_permissions() {
  $permissions = array();

  // Exported permission: Allow Reordering
  $permissions['Allow Reordering'] = array(
    'name' => 'Allow Reordering',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'content manager',
      '2' => 'tracks sorter',
    ),
  );

  // Exported permission: create podcast_collection content
  $permissions['create podcast_collection content'] = array(
    'name' => 'create podcast_collection content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'content manager',
    ),
  );

  // Exported permission: create podcast_item content
  $permissions['create podcast_item content'] = array(
    'name' => 'create podcast_item content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'content manager',
    ),
  );

  // Exported permission: delete own podcast_collection content
  $permissions['delete own podcast_collection content'] = array(
    'name' => 'delete own podcast_collection content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete own podcast_item content
  $permissions['delete own podcast_item content'] = array(
    'name' => 'delete own podcast_item content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit any podcast_collection content
  $permissions['edit any podcast_collection content'] = array(
    'name' => 'edit any podcast_collection content',
    'roles' => array(
      '0' => 'content manager',
    ),
  );

  // Exported permission: edit any podcast_item content
  $permissions['edit any podcast_item content'] = array(
    'name' => 'edit any podcast_item content',
    'roles' => array(
      '0' => 'content manager',
    ),
  );

  // Exported permission: revert revisions
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      '0' => 'content manager',
    ),
  );

  // Exported permission: view revisions
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      '0' => 'content manager',
    ),
  );

  return $permissions;
}
