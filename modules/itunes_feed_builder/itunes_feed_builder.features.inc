<?php
/**
 * Implementation of hook_ctools_plugin_api().
 */
function itunes_feed_builder_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_default_fe_taxonomy_vocabulary().
 */
function itunes_feed_builder_default_fe_taxonomy_vocabulary() {
  $export = array();

  $fe_taxonomy_vocabulary = new stdClass;
  $fe_taxonomy_vocabulary->name = 'Keywords';
  $fe_taxonomy_vocabulary->description = '';
  $fe_taxonomy_vocabulary->help = '';
  $fe_taxonomy_vocabulary->relations = '1';
  $fe_taxonomy_vocabulary->hierarchy = '0';
  $fe_taxonomy_vocabulary->multiple = '0';
  $fe_taxonomy_vocabulary->required = '0';
  $fe_taxonomy_vocabulary->tags = '1';
  $fe_taxonomy_vocabulary->module = 'taxonomy';
  $fe_taxonomy_vocabulary->weight = '0';
  $fe_taxonomy_vocabulary->nodes = array(
    'podcast_collection' => 'podcast_collection',
    'podcast_item' => 'podcast_item',
  );
  $fe_taxonomy_vocabulary->machine_name = 'keywords';

  $export['keywords'] = $fe_taxonomy_vocabulary;

  return $export;
}

/**
 * Implementation of hook_node_info().
 */
function itunes_feed_builder_node_info() {
  $items = array(
    'podcast_collection' => array(
      'name' => t('Collection'),
      'module' => 'features',
      'description' => t('A group of tracks/items that appear as a single podcast. For example: a series of lectures for a particular class; a series of episodes of a program, etc.'),
      'has_title' => '1',
      'title_label' => t('Collection name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'podcast_item' => array(
      'name' => t('Track'),
      'module' => 'features',
      'description' => t('A single file inside a podcast.'),
      'has_title' => '1',
      'title_label' => t('Item title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function itunes_feed_builder_views_api() {
  return array(
    'api' => '2',
  );
}
