<?php
/**
 * Implementation of hook_views_default_views().
 */
function itunes_feed_builder_views_default_views() {
  $views = array();

  // Exported view: all_items
  $view = new view;
  $view->name = 'all_items';
  $view->description = 'Podcast collections and items';
  $view->tag = 'podcast';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'view_node' => array(
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => 'view',
    ),
    'edit_node' => array(
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'label' => '',
      'alter' => array(
        'alter_text' => 1,
        'text' => ' [edit_node]',
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
    ),
    'field_image_url' => array(
      'id' => 'field_image_url',
      'table' => 'node_data_field_image',
      'field' => 'field_image_url',
      'label' => 'Collection image URL',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 1,
        'max_length' => '1000',
        'word_boundary' => 0,
        'ellipsis' => 0,
        'strip_tags' => 1,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
    ),
    'title' => array(
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<h3>[title]</h3>',
        'make_link' => 0,
        'trim' => 1,
        'max_length' => '80',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
    ),
    'changed' => array(
      'id' => 'changed',
      'table' => 'node',
      'field' => 'changed',
      'label' => 'Last update date',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 0,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
    ),
    'name' => array(
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'label' => 'Creator',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => '1',
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'podcast_collection' => 'podcast_collection',
      ),
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'columns' => array(
      'view_node' => 'view_node',
      'edit_node' => 'view_node',
      'field_image_url' => 'title',
      'title' => 'title',
      'changed' => 'changed',
      'name' => 'name',
    ),
    'default' => 'changed',
    'info' => array(
      'view_node' => array(
        'align' => '',
        'separator' => '',
      ),
      'edit_node' => array(
        'align' => '',
        'separator' => '',
      ),
      'field_image_url' => array(
        'sortable' => 0,
        'align' => '',
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 1,
        'align' => '',
        'separator' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'align' => '',
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'align' => '',
        'separator' => '',
      ),
    ),
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
  ));
  $handler = $view->new_display('page', 'Collections', 'page_1');
  $handler->override_option('path', 'all/collections');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'All Collections',
    'description' => '',
    'weight' => '0',
    'name' => 'primary-links',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'normal',
    'title' => 'All items',
    'description' => '',
    'weight' => '0',
    'name' => 'primary-links',
  ));
  $handler = $view->new_display('page', 'Items', 'page_2');
  $handler->override_option('fields', array(
    'view_node' => array(
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => 'view',
    ),
    'edit_node' => array(
      'id' => 'edit_node',
      'table' => 'node',
      'field' => 'edit_node',
      'label' => '',
      'alter' => array(
        'alter_text' => 1,
        'text' => ' [edit_node]',
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
    ),
    'title' => array(
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<h3>[title]</h3>',
        'make_link' => 0,
        'trim' => 1,
        'max_length' => '80',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
    ),
    'field_duration_value' => array(
      'id' => 'field_duration_value',
      'table' => 'node_data_field_duration',
      'field' => 'field_duration_value',
      'label' => 'Duration',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
    ),
    'changed' => array(
      'id' => 'changed',
      'table' => 'node',
      'field' => 'changed',
      'label' => 'Last update date',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 0,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
    ),
    'name' => array(
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'label' => 'Creator',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
    ),
    'field_parent_nid' => array(
      'id' => 'field_parent_nid',
      'table' => 'node_data_field_parent',
      'field' => 'field_parent_nid',
      'label' => 'Belongs to collection',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => '1',
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'podcast_item' => 'podcast_item',
      ),
    ),
    'keys' => array(
      'id' => 'keys',
      'table' => 'search_index',
      'field' => 'keys',
      'operator' => 'optional',
      'expose' => array(
        'operator' => 'keys_op',
        'use_operator' => 0,
        'identifier' => 'keys',
      ),
    ),
  ));
  $handler->override_option('path', 'all/tracks');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'All Tracks',
    'description' => '',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  // Exported view: podcast_collection_items
  $view = new view;
  $view->name = 'podcast_collection_items';
  $view->description = 'Shows the tracks/items for a particular podcast collection.';
  $view->tag = 'podcast';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 1,
        'max_length' => '120',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
    ),
    'value0' => array(
      'id' => 'value0',
      'table' => 'draggableviews_structure_node0',
      'field' => 'value0',
      'exclude' => TRUE,
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
    ),
    'field_duration_value' => array(
      'id' => 'field_duration_value',
      'table' => 'node_data_field_duration',
      'field' => 'field_duration_value',
      'label' => 'Duration',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
    ),
    'phpcode' => array(
      'id' => 'phpcode',
      'table' => 'customfield',
      'field' => 'phpcode',
      'label' => 'File size',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'value' => '<?php
$extra = filefieldftpdest_load($data->nid);
echo sprintf("%.1f MB", $extra[\'field_mediafile\'][0][\'filesize\'] / 1048576);
?>',
    ),
    'field_summary_value' => array(
      'id' => 'field_summary_value',
      'table' => 'node_data_field_summary',
      'field' => 'field_summary_value',
      'label' => 'Summary',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'trim' => 1,
        'max_length' => '200',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
    ),
  ));
  $handler->override_option('sorts', array(
    'value0' => array(
      'id' => 'value0',
      'table' => 'draggableviews_structure_node0',
      'field' => 'value0',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_parent_nid' => array(
      'id' => 'field_parent_nid',
      'table' => 'node_data_field_parent',
      'field' => 'field_parent_nid',
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'title' => 'Tracks/items in collection %1',
      'default_argument_type' => 'node',
      'validate_type' => 'node',
      'validate_options' => array(
        'types' => array(
          'podcast_collection' => 'podcast_collection',
        ),
        'access' => 1,
      ),
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'value' => '1',
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'value' => array(
        'podcast_item' => 'podcast_item',
      ),
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'draggabletable');
  $handler->override_option('style_options', array(
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'value0' => 'value0',
      'field_duration_value' => 'field_duration_value',
      'phpcode' => 'phpcode',
      'field_summary_value' => 'field_summary_value',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'value0' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_duration_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'phpcode' => array(
        'separator' => '',
      ),
      'field_summary_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => -1,
    'tabledrag_order' => array(
      'field' => 'value0',
      'handler' => 'native',
    ),
    'tabledrag_order_visible' => array(
      'visible' => 0,
    ),
    'tabledrag_hierarchy' => array(
      'field' => 'none',
      'handler' => 'native',
    ),
    'tabledrag_hierarchy_visible' => array(
      'visible' => 0,
    ),
    'draggableviews_depth_limit' => '0',
    'draggableviews_repair' => array(
      'repair' => 'repair',
    ),
    'tabledrag_types_add' => 'Add type',
    'tabledrag_expand' => array(
      'expand_links' => 0,
      'collapsed' => 0,
      'by_uid' => 0,
    ),
    'draggableviews_extensions' => array(
      'extension_top' => '3',
      'extension_bottom' => '3',
    ),
    'tabledrag_lock' => array(
      'lock' => 0,
    ),
    'draggableviews_default_on_top' => '1',
    'draggableviews_button_text' => 'Save order',
    'draggableviews_arguments' => array(
      'use_args' => 0,
    ),
  ));
  $handler = $view->new_display('block', 'Block: items in current collection', 'block_1');
  $handler->override_option('arguments', array(
    'field_parent_nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => 'Tracks/items in collection %1',
      'breadcrumb' => '',
      'default_argument_type' => 'node',
      'default_argument' => '',
      'validate_type' => 'node',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_parent_nid',
      'table' => 'node_data_field_parent',
      'field' => 'field_parent_nid',
      'validate_options' => array(
        'types' => array(
          'podcast_collection' => 'podcast_collection',
        ),
        'access' => 0,
      ),
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '3' => 0,
      ),
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'podcast_collection' => 'podcast_collection',
        'podcast_item' => 0,
        'panel' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
        '2' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('feed', 'Feed', 'feed_1');
  $handler->override_option('arguments', array(
    'field_parent_nid' => array(
      'id' => 'field_parent_nid',
      'table' => 'node_data_field_parent',
      'field' => 'field_parent_nid',
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'title' => '%1',
      'default_argument_type' => 'node',
      'validate_type' => 'node',
      'validate_options' => array(
        'types' => array(
          'podcast_collection' => 'podcast_collection',
        ),
        'access' => 1,
      ),
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
    ),
  ));
  $handler->override_option('style_plugin', 'rss');
  $handler->override_option('style_options', array(
    'mission_description' => FALSE,
    'description' => '',
  ));
  $handler->override_option('row_plugin', 'node_rss');
  $handler->override_option('path', 'node/%/itunesfeed');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('displays', array(
    'default' => 0,
    'block_1' => 0,
  ));
  $handler->override_option('sitename_title', FALSE);

  $views[$view->name] = $view;

  return $views;
}