<?php
/**
 * Implementation of hook_strongarm().
 */
function itunes_feed_builder_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'active_tags_1';
  $strongarm->value = 0;

  $export['active_tags_1'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'active_tags_2';
  $strongarm->value = 0;

  $export['active_tags_2'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'arrange_fields_position_data_podcast_collection_node_form';
  $strongarm->value = 'edit-title-draggable-wrapper,0px,0px,input,383px,14px,42%,,,top,;edit-field-subtitle-draggable-wrapper,50px,0px,input,380px,14px,42%,,,top,;edit-field-summary-draggable-wrapper,240px,0px,textarea,377px,113px,42%,,,top,;edit-field-language-draggable-wrapper,230px,430px,,0px,0px,,,,,;edit-field-keywords-draggable-wrapper,330px,430px,input,254px,16px,42%,,,top,;edit-field-image-draggable-wrapper,480px,430px,input,385px,14px,42%,,,top,;edit-ftpdest-draggable-wrapper,610px,0px,input,247px,16px,42%,,,top,;edit-field-category-draggable-wrapper,0px,430px,,0px,0px,42%,,,top,;edit-field-explicit-draggable-wrapper,60px,430px,,0px,0px,42%,,,top,;edit-menu-fieldset-draggable-wrapper,850px,0px,,0px,0px,,,,,;edit-path-fieldset-draggable-wrapper,880px,0px,,0px,0px,,,,,;edit-field-authors-draggable-wrapper,140px,0px,input,380px,14px,42%,,,top,;edit-field-chan-owner-name-draggable-wrapper,530px,0px,input,380px,14px,42%,,,top,;edit-field-chan-owner-email-draggable-wrapper,450px,0px,input,380px,14px,42%,,,top,;edit-field-collection-thumbnail-draggable-wrapper,610px,430px,,0px,0px,42%,,,top,;edit-author-fieldset-draggable-wrapper,790px,0px,,0px,0px,,,,,;edit-options-fieldset-draggable-wrapper,820px,0px,,0px,0px,,,,,;edit-buttons-draggable-wrapper,690px,0px,,0px,0px,,,,top,;~~maxBottom~~,944px';

  $export['arrange_fields_position_data_podcast_collection_node_form'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'arrange_fields_position_data_podcast_item_node_form';
  $strongarm->value = 'edit-title-draggable-wrapper,0px,0px,input,387px,21px,43%,,,top,;edit-field-subtitle-draggable-wrapper,150px,0px,input,372px,14px,40%,,,top,;edit-field-authors-draggable-wrapper,50px,0px,input,380px,14px,,,,,;edit-field-summary-draggable-wrapper,70px,450px,textarea,351px,113px,40%,,,top,;edit-menu-fieldset-draggable-wrapper,690px,0px,,0px,0px,,,,,;edit-field-keywords-draggable-wrapper,360px,450px,input,254px,16px,,,,,;edit-field-category-draggable-wrapper,0px,450px,,0px,0px,,,,,;edit-field-link-draggable-wrapper,240px,0px,input,367px,14px,,,,,;edit-field-duration-draggable-wrapper,330px,0px,input,68px,14px,,,,,;edit-field-parent-draggable-wrapper,290px,450px,,0px,0px,,,,,;edit-field-mediafile-draggable-wrapper,400px,0px,,0px,0px,,,,,;edit-path-fieldset-draggable-wrapper,730px,0px,,0px,0px,,,,,;edit-author-fieldset-draggable-wrapper,780px,0px,,0px,0px,,,,,;edit-options-fieldset-draggable-wrapper,820px,0px,,0px,0px,,,,,;edit-buttons-draggable-wrapper,560px,0px,,0px,0px,,,,,;~~maxBottom~~,849px';

  $export['arrange_fields_position_data_podcast_item_node_form'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_podcast_collection';
  $strongarm->value = 0;

  $export['comment_anonymous_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_podcast_item';
  $strongarm->value = 0;

  $export['comment_anonymous_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_podcast_collection';
  $strongarm->value = '3';

  $export['comment_controls_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_podcast_item';
  $strongarm->value = '3';

  $export['comment_controls_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_podcast_collection';
  $strongarm->value = '4';

  $export['comment_default_mode_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_podcast_item';
  $strongarm->value = '4';

  $export['comment_default_mode_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_podcast_collection';
  $strongarm->value = '1';

  $export['comment_default_order_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_podcast_item';
  $strongarm->value = '1';

  $export['comment_default_order_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_podcast_collection';
  $strongarm->value = '50';

  $export['comment_default_per_page_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_podcast_item';
  $strongarm->value = '50';

  $export['comment_default_per_page_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_podcast_collection';
  $strongarm->value = '0';

  $export['comment_form_location_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_podcast_item';
  $strongarm->value = '0';

  $export['comment_form_location_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_podcast_collection';
  $strongarm->value = '0';

  $export['comment_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_podcast_item';
  $strongarm->value = '0';

  $export['comment_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_podcast_collection';
  $strongarm->value = '1';

  $export['comment_preview_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_podcast_item';
  $strongarm->value = '1';

  $export['comment_preview_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_podcast_collection';
  $strongarm->value = '1';

  $export['comment_subject_field_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_podcast_item';
  $strongarm->value = '1';

  $export['comment_subject_field_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_podcast_collection';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '5',
    'menu' => '4',
    'taxonomy' => '3',
    'path' => '6',
  );

  $export['content_extra_weights_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_podcast_item';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '6',
    'menu' => '5',
    'taxonomy' => '4',
    'path' => '7',
  );

  $export['content_extra_weights_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'draggableviews_repaired_msg';
  $strongarm->value = '';

  $export['draggableviews_repaired_msg'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'file_directory_path';
  $strongarm->value = 'sites/default/files';

  $export['file_directory_path'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'getid3_path';
  $strongarm->value = 'sites/all/libraries/getid3/getid3';

  $export['getid3_path'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'getid3_show_warnings';
  $strongarm->value = 1;

  $export['getid3_show_warnings'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_podcast_collection';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );

  $export['node_options_podcast_collection'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_podcast_item';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );

  $export['node_options_podcast_item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_footer';
  $strongarm->value = '';

  $export['site_footer'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'all';

  $export['site_frontpage'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'iTunes Manager';

  $export['site_name'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_slogan';
  $strongarm->value = '';

  $export['site_slogan'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = TRUE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'itunesy';

  $export['theme_default'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = TRUE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_itunesy_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 0,
    'toggle_mission' => 1,
    'toggle_node_user_picture' => 0,
    'toggle_comment_user_picture' => 0,
    'toggle_search' => 0,
    'toggle_favicon' => 1,
    'toggle_primary_links' => 1,
    'toggle_secondary_links' => 1,
    'default_logo' => 0,
    'logo_path' => 'profiles/itunesuprofile/themes/itunesy/itunesy_favicon.png',
    'logo_upload' => '',
    'default_favicon' => 0,
    'favicon_path' => 'profiles/itunesuprofile/themes/itunesy/itunesy_favicon.png',
    'favicon_upload' => '',
  );

  $export['theme_itunesy_settings'] = $strongarm;
  return $export;
}
