<?php
/**
 * Implementation of hook_user_default_roles().
 */
function itunes_feed_builder_user_default_roles() {
  $roles = array();

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  // Exported role: authenticated user
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
  );

  // Exported role: tracks sorter
  $roles['tracks sorter'] = array(
    'name' => 'tracks sorter',
  );

  return $roles;
}
