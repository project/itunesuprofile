<?php
/**
 * Implementation of hook_menu_default_menu_links().
 */
function itunes_feed_builder_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:all
  $menu_links['primary-links:all'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'all',
    'router_path' => 'all',
    'link_title' => 'All items',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: primary-links:logout
  $menu_links['primary-links:logout'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'logout',
    'router_path' => 'logout',
    'link_title' => 'Log out',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '10',
  );
  // Exported menu link: primary-links:user/%
  $menu_links['primary-links:user/%'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'user/%',
    'router_path' => 'user/%',
    'link_title' => 'My account',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('All items');
  t('Log out');
  t('My account');


  return $menu_links;
}
